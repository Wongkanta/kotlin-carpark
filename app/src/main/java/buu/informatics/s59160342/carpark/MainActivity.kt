package buu.informatics.s59160342.carpark

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import buu.informatics.s59160342.carpark.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var carPark : ArrayList<CarPark> = ArrayList()
    var index = 0;

    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.apply {
            update_button.setOnClickListener{addData(it)}
            edit_button.setOnClickListener { editData(it) }
            Slot1.setOnClickListener { showData(it , 1) }
            Slot2.setOnClickListener { showData(it , 2) }
            Slot3.setOnClickListener { showData(it , 3) }
        }

        for(i in 1..3){
            var CarPark : CarPark = CarPark("","","")
            carPark.add(CarPark)
        }
    }

    private fun showData(view:View,slot: Int){
        binding.apply {
            textView_license.visibility = View.VISIBLE
            textView_car.visibility = View.VISIBLE
            textView_name.visibility = View.VISIBLE

            if(slot == 1){
                textView_license.text = carPark.get(0).license
                textView_car.text = carPark.get(0).car
                textView_name.text = carPark.get(0).name
                index = 0
            }else if (slot == 2){
                textView_license.text = carPark.get(1).license
                textView_car.text = carPark.get(1).car
                textView_name.text = carPark.get(1).name
                index = 1
            }else if (slot == 3){
                textView_license.text = carPark.get(2).license
                textView_car.text = carPark.get(2).car
                textView_name.text = carPark.get(2).name
                index = 2
            }
        }
    }

    private fun addData(view: View){
        binding.apply {
            textView_license.text = edit_license.text
            textView_car.text = edit_car.text
            textView_name.text = edit_name.text

            edit_license.visibility = View.GONE
            edit_car.visibility = View.GONE
            edit_name.visibility = View.GONE

            textView_license.visibility = view.visibility
            textView_car.visibility = view.visibility
            textView_name.visibility = view.visibility


            var CarPark : CarPark = CarPark(textView_license.text.toString(),textView_car.text.toString(),textView_name.text.toString())
            carPark.set(index,CarPark)

            edit_license.setText("")
            edit_car.setText("")
            edit_name.setText("")
        }

    }

    private fun editData(view: View){
        binding.apply {

            textView_license.text = edit_license.text
            textView_car.text = edit_car.text
            textView_name.text = edit_name.text

            edit_license.visibility  = View.VISIBLE
            edit_car.visibility  = View.VISIBLE
            edit_name.visibility  = View.VISIBLE


            textView_license.visibility = View.GONE
            textView_car.visibility = View.GONE
            textView_name.visibility = View.GONE
        }
    }

}
